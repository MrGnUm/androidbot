package com.mrgnum.webviewtest;

/**
 * Created by Mr.GnUm on 27.05.2015.
 */

import android.content.Context;
import android.util.Log;
import android.webkit.WebView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Класс таймера для бота, что бы он запускал после загрузки страницы его постоянно
 */
class myBotTimerTask extends TimerTask
{
    private String url;
    private Timer mTimer;
    private Context mContext;
    private WebView myBrowser;
    myBotTimerTask(String u, Timer t, Context c, WebView wb)
    {
        url = u;
        mTimer = t;
        myBrowser = wb;
        mContext = c;
    }
    @Override
    public void run()
    {
        Log.d("debug", "timer worked");
        BotTask task = new BotTask(mContext, myBrowser, url, mTimer);
        task.execute();
    }
}

