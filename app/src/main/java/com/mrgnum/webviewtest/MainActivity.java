package com.mrgnum.webviewtest;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ProgressBar;


public class MainActivity extends ActionBarActivity {
    private WebView myBrowser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        JavaScriptInterface JSI = new JavaScriptInterface(this);

        myBrowser = (WebView) findViewById(R.id.webView);
        myBrowser.setWebViewClient(new MyWebViewClient(this, myBrowser));

        ProgressBar pb = (ProgressBar) findViewById(R.id.progressBar);
        myBrowser.setWebChromeClient(new MyWebChromeClient(pb));
        myBrowser.getSettings().setJavaScriptEnabled(true);
        myBrowser.getSettings().setDomStorageEnabled(true);
        myBrowser.getSettings().setSupportMultipleWindows(false);

        myBrowser.addJavascriptInterface(JSI, "jsAPI");

        // если Login и Password не определены (не сохранены) отправляем в настройки
        if(isExistLoginPassword())
        {
            loadPage();
        }
        else
        {
            Intent intent = new Intent(MainActivity.this, Settings.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id)
        {
            case R.id.action_settings:
                Intent intent = new Intent(MainActivity.this, Settings.class);
                startActivity(intent);
                return true;
            case R.id.js:
                Act();
                return true;
            case R.id.exit:
                System.exit(0);
                return true;
            case R.id.go:
                loadPage();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if(myBrowser.canGoBack()) {
            myBrowser.goBack();
        }
    }

    /**
     * загрузка начальной страницы
     */
    private void loadPage() {
        myBrowser.loadUrl("http://www.ganjawars.ru/me/");
    }

    private boolean isExistLoginPassword()
    {
        String login = "";
        String pass = "";
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if(sharedPref.contains(getString(R.string.login)))
        {
            login = sharedPref.getString(getString(R.string.login), null);
        }
        if(sharedPref.contains(getString(R.string.pass)))
        {
            pass = sharedPref.getString(getString(R.string.pass), null);
        }
        // существует ли Login и Password тогда что-то делаем =)
        return (login != null && !login.isEmpty() && pass != null && !pass.isEmpty());
    }

    private void Act(){
//        loadPage();
        if (!isExistLoginPassword()) {
            Intent intent = new Intent(MainActivity.this, Settings.class);
            startActivity(intent);
        } else {
            Log.d("debug", myBrowser.getUrl());
//            BotTask task = new BotTask(this, myBrowser, myBrowser.getUrl());
//            task.execute();
        }
    }
}

