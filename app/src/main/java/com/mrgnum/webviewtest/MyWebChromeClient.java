package com.mrgnum.webviewtest;

/**
 * Created by Mr.GnUm on 20.05.2015.
 */

import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

/**
 * Расширение класса WebChromeClient
 */
public class MyWebChromeClient extends WebChromeClient
{
    private ProgressBar Pbar;

    MyWebChromeClient(ProgressBar pb)
    {
        Pbar = pb;
    }

    public void onProgressChanged(WebView view, int progress)
    {
        if(progress < 100 && Pbar.getVisibility() == ProgressBar.GONE){
            Pbar.setVisibility(ProgressBar.VISIBLE);
        }
        Pbar.setProgress(progress);
        if(progress == 100) {
            Pbar.setVisibility(ProgressBar.GONE);
        }
    }
}
