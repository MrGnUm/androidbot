package com.mrgnum.webviewtest;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.webkit.WebView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;

/**
 * Created by Mr.GnUm on 25.05.2015.
 */
public class BotTask extends AsyncTask<Void, Void, Void> {

    private Document document;
    private SharedPreferences sharedPref;
    private Context mContext;
    private WebView myBrowser;
    private URL myUrl;
    private String jsCode;
    boolean jsExecute;
    private String htmlPage;
    private Timer mTimer;

    BotTask(Context c, WebView wb, String u, Timer t)
    {
        mContext = c;
        myBrowser = wb;
        mTimer = t;
        try {
            myUrl = new URL(u);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext.getApplicationContext());
        jsCode = "";
        jsExecute = false;
        htmlPage = getHtmlFromFile();
    }

    @Override
    protected void onPreExecute(){

    }

    @Override
    protected Void doInBackground(Void... params) {

        // если текст html еще не считался, выходим и ничего не делаем
        if(htmlPage.length() == 0) return null;

        mTimer.cancel();

        Log.d("debug","all data: " + sharedPref.getAll());
        Log.d("debug",myUrl.getPath());

        switch(myUrl.getPath())
        {
            case "/login.php":
                login();
                break;

            case "/me/":
                getWork();
                break;

            default: break;
        }
        return null;
    }


    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

        if(jsExecute)
        {
            Log.d("debug", jsCode);
            myBrowser.loadUrl("javascript: "+jsCode);
            jsExecute = false;
        }
        Log.i("debug", "Finish in Background");
    }

    private void login()
    {
        document = null;//Здесь хранится будет разобранный html документ
        document = Jsoup.parse(htmlPage);

        String login = "";
        String pass = "";
        if(sharedPref.contains(mContext.getResources().getString(R.string.login)))
        {
            login = sharedPref.getString(mContext.getResources().getString(R.string.login), null);
        }
        if(sharedPref.contains(mContext.getResources().getString(R.string.pass)))
        {
            pass = sharedPref.getString(mContext.getResources().getString(R.string.pass), null);
        }

        // Если документ загрузился и существует Login и Password тогда что-то делаем =)
        if (document !=null && login != null && !login.isEmpty() && pass != null && !pass.isEmpty())
        {
            // вызываем метод JS login(login,pass)
            if(document.select("#hpheader").isEmpty())
            {
                jsCode = "login('"+login+"','"+pass+"');";
                jsExecute = true;
            }
        }
    }

    private void getWork()
    {
        Log.d("debug","getWork");
        Log.d("debug", Integer.toString(htmlPage.length()));

//                Elements inputs = document.select("#myform input[type!=hidden]");
//                for (Element input : inputs) {
//                    Log.d("debug",input.toString());
//                    if(Objects.equals(input.attr("name"), "login"))
//                    {
//                        input.attr("value",login);
//                    }
//                    if(Objects.equals(input.attr("name"), "pass"))
//                    {
//                        input.attr("value",pass);
//                    }
//                }



    }


    private String getHtmlFromFile()
    {
        StringBuilder sb = new StringBuilder();
        File file = new File(mContext.getFilesDir(), mContext.getString(R.string.htmlPage));

        if(file.exists() && file.canRead())
        {
            try {
                //Объект для чтения файла в буфер
                BufferedReader in = new BufferedReader(new FileReader( file.getAbsoluteFile()));
                try {
                    //В цикле построчно считываем файл
                    String s;
                    while ((s = in.readLine()) != null) {
                        sb.append(s);
                        sb.append("\n");
                    }
                    Log.d("debug","read file");
                } finally {
                    //Также не забываем закрыть файл
                    in.close();
                    Log.d("debug","close file");
                }
            } catch(IOException e) {
                throw new RuntimeException(e);
            }
            if(file.delete())
            {
                Log.d("debug","file deleted");
            }
        }
        else
        {
            Log.d("debug","file not exist or can't read");
        }
        return (sb.toString().isEmpty())? "" : sb.toString();
    }
}