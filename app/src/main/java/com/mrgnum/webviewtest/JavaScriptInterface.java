package com.mrgnum.webviewtest;

import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Mr.GnUm on 16.05.2015.
 */
public class JavaScriptInterface {

    Context mContext;

    /** Instantiate the interface and set the context */

    JavaScriptInterface(Context c) {

        mContext = c;

    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast(String toast) {

        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
        Log.d("debug","Ready");

    }

    @JavascriptInterface
    public void writeHtmlToFile(String html)
    {

        Log.d("debug",Integer.toString(html.length()));
        File file = new File(mContext.getFilesDir(), mContext.getString(R.string.htmlPage));
        // Запсиываем файл
        try {
            //проверяем, что если файл не существует то создаем его
            if(!file.exists())
            {
                file.createNewFile();
            }
            //PrintWriter обеспечит возможности записи в файл
            PrintWriter out = new PrintWriter(file.getAbsoluteFile());
            try  {
                out.print(html);
                Log.d("debug","write file");
            }finally {
                out.close();
                Log.d("debug","close file");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
