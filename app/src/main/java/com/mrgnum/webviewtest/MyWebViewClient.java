package com.mrgnum.webviewtest;

/**
 * Created by Mr.GnUm on 20.05.2015.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Расширение класса WebViewClient для браузера
 */
public class MyWebViewClient extends WebViewClient
{
    private WebView myBrowser;
    private Context mContext;
    private boolean isError;
    private Timer mTimer;

    MyWebViewClient(Context c, WebView webView)
    {
        mContext = c;
        myBrowser = webView;
    }

    public void onPageStarted (WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view,url,favicon);
        isError = false;
        Log.d("debug", "start: " +url);

    }
    public void onPageFinished(WebView view, String url)
    {
        super.onPageFinished(view,url);
        if(!isError)
        {
            loadMainJavaScript();
            Log.d("debug", "Finish: " + url);

            if(mTimer != null)
            {
                mTimer.cancel();
            }
            mTimer = new Timer();

            myBotTimerTask botTimer = new myBotTimerTask(myBrowser.getUrl(), mTimer, mContext, myBrowser);
            mTimer.schedule(botTimer, 0, 1000);
        }
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        Log.d("debug","error code:" + description);
        isError = true;
        super.onReceivedError(view, errorCode, description, failingUrl);

        // вывод ошибки в Диалоговом окне
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(mContext);
        dlgAlert.setMessage(description);
        dlgAlert.setTitle("Ошибка:" + errorCode);
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url)
    {
        if(url.startsWith("http://www.ganjawars.ru") )
        {
            view.loadUrl(url);
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Загрузка основого JavaScript
     */
    private void loadMainJavaScript()
    {
        String mainJSFile = "js/script.js";
        StringBuilder buf = new StringBuilder();
        InputStream js;
        try {
            js = mContext.getAssets().open(mainJSFile);
            BufferedReader in = new BufferedReader(new InputStreamReader(js, "UTF-8"));
            String str;
            while ((str=in.readLine()) != null) {
                buf.append(str);
            }

            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        myBrowser.loadUrl("javascript: " + buf.toString());
    }
}



