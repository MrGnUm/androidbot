package com.mrgnum.webviewtest;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;


public class Settings extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        fillFields();
    }

    public void saveSettings(View view) {
        EditText editText = (EditText) findViewById(R.id.editText_Login);
        String login = editText.getText().toString();
        editText = (EditText) findViewById(R.id.editText_pass);
        String pass = editText.getText().toString();

        if(!login.isEmpty() && !pass.isEmpty())
        {
            Log.d("debug", "not empty");

            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(getString(R.string.login), login);
            editor.putString(getString(R.string.pass), pass);
            editor.apply();
            finish();
        }
        else
        {
            Log.d("debug","empty =(");
            AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
            dlgAlert.setMessage("Пожалуйста введи Логин и Пароль");
            dlgAlert.setTitle("Ошибка =(");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
        }
    }

    public void fillFields(){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String login = "";
        String pass = "";
        if(sharedPref.contains(getString(R.string.login)))
        {
            login = sharedPref.getString(getString(R.string.login), null);
        }
        if(sharedPref.contains(getString(R.string.pass)))
        {
            pass = sharedPref.getString(getString(R.string.pass), null);
        }
        if(login !=null && !login.isEmpty() && pass != null && !pass.isEmpty())
        {
            Log.d("debug", "not empty on load, login=" + login + " pass="+pass);
            EditText editText = (EditText) findViewById(R.id.editText_Login);
            editText.setText(login);
        }
        else
        {
            Log.d("debug","empty on load =(");
        }
    }

    /* Close */
    public void cancelSettings(View view) { finish(); }
}
